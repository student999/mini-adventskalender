var mysql = require('mysql')
const express = require('express')
const app = express()
app.set('view engine', 'ejs')
app.use(express.static('public'))

const server = app.listen(process.env.PORT || 3000, () => {
    console.log('Server lauscht auf Port %s', server.address().port)    
})

var con = mysql.createConnection({
    host: "130.255.124.99", //  10.40.38.110 
    user: "weihnachtsmann",
    password: "nikolaus",
    database: "db2412"
})

app.get('/',function(req,res){    
    console.log("Webseite angefordert")
    if(new Date().getMonth() == 11 && new Date().getDate() <= 24 ){        
        res.render('index.ejs',{
            buttonbeschriftung:"Türchen Nr. " + new Date().getDate() + " öffnen",            
            kalenderspruch: ""
        })    
    }else{    
        res.render('index.ejs',{
            buttonbeschriftung:"Keine Adventszeit!",            
            kalenderspruch: ""
        })
    }    
})

app.post('/', function(req,res){    
    console.log("Button geklickt")
    if(new Date().getMonth() == 11 && new Date().getDate() <= 24 ){
        con.query("SELECT eintrag from kalendereintraege WHERE tag = '" + new Date().getDate() + "';", function(err, result) {
            if (err) {
                res.render('index.ejs', {
                    buttonbeschriftung: "Noch " + (24 - new Date().getDate()) + " Tage bis Weihnachten",
                    kalenderspruch:err.message    
                })
                return console.error('Fehler: ' + err.message);
            }else{
                if(result != ""){
                    res.render('index.ejs', {
                        buttonbeschriftung: "Noch " + (24 - new Date().getDate()) + " Tage bis Weihnachten",
                        kalenderspruch:result[0][Object.keys(result[0])[0]]
                    })                           
                }else{
                    res.render('index.ejs', {
                        buttonbeschriftung: "Noch " + (24 - new Date().getDate()) + " Tage bis Weihnachten",
                        kalenderspruch:"Kein Eintrag in der Datenbank."
                    })                           
                }
            }
        })
    }else{
        res.render('index.ejs', {
            buttonbeschriftung: "Keine Adventszeit!",
            kalenderspruch:""
        })                           
    }
})